﻿using System;
using System.Collections.Generic;

namespace external
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            List<int> list = new List<int> { 1,2,3,4,5,6 };
            var enumerator_number = list.GetEnumerator();
            while (enumerator_number.MoveNext()) {
                int number = enumerator_number.Current;
                Console.Write(number + " ");
            }
        }
    }
}
