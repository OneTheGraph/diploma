﻿using System;
using System.Collections;

namespace our
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Week week = new Week();

            foreach (string day in week) {
                Console.Write(day + " ");
            }
        }
    }

    public class Week : IEnumerable
    {
        private string[] days = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

    public IEnumerator GetEnumerator()
        {
            for(int i = 0; i < days.Length;  i++) {
                yield return days[i];
            }
        }
    }
}
