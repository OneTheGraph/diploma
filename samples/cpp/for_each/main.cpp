#include <iostream>
#include <iterator>
#include <list>
#include <ranges>
#include <vector>

template <class Type>
void print(std::vector<Type> vec);

template <class Type>
void plusOne(std::vector<Type> &vec);

int main() {
  // Инициализация вектора.
  std::vector<int> v{1, 2, 3, 4, 5};
  // Печатаем.
  print(v);
  // Прибавляем ко всем элементам 1.
  plusOne(v);
  // Проверяем результат.
  print(v);

  return 0;
}

template <class Type>
void print(std::vector<Type> vec) {
  // Проходимся по всему вектору, при помощи специально предназначенного для
  // этого итератора ostream_iterator выводим элементы.
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;
}

template <class Type>
void plusOne(std::vector<Type> &vec) {
	// Инициализируем лямбда-выражение.
  auto plus_one = [](Type &x) { x++; };
	// В for_each() можно использовать лямбда-выражения.
  std::for_each(vec.begin(), vec.end(), plus_one);
  std::cout << std::endl;
}
