# foreach
В примере показано внутреннее (неявное) использование итераторов для разных структур данных, содержащих разные типы данных.

# Запуск проекта
Для запуска проекта необходимо установить `make`, `g++ >= 8.0`, `cmake`, `build-essential`, `checkinstall`.

**Внимание!** В проекте используется стандарт `C++20`, убедитесь, что ваш бэкенд языка (`Clang`) или компилятор (`G++`) поддерживает этот стандарт! 

`Ubuntu`: `sudo apt install make g++ cmake build-essential checkinstall` 

# Компиляция:
В каталоге проекта выполните команду:

`cmake . && make`

# Запуск:
Запуск проекта осуществляется при помощи команды:

`./foreach`
