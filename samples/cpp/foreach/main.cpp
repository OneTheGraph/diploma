#include <iostream>
#include <iterator>
#include <list>
#include <vector>
#include <ranges>

template <class Data>
void directOrderPrintInTerminal(Data data);

template <typename Data>
void reverseOrderPrintInTerminal(Data data);

int main() {
  // Инициализация векторов
  std::vector<int> vector = {1, 2, 3, 4, 5, 6};
  std::vector<char> char_vector = {'Q', 'u', 'i', 'x', 'o', 't', 'e'};

  std::cout << "For vectors:" << std::endl;
  // Вызов функций для одинаковых контейнеров (структур данных),
  // но разных типов данных для печати на экране в прямом порядке
  directOrderPrintInTerminal(vector);
  directOrderPrintInTerminal(char_vector);
  // Вызов функций для одинаковых контейнеров (структур данных),
  // но разных типов данных для печати на экране в обратном порядке
  reverseOrderPrintInTerminal(vector);
  reverseOrderPrintInTerminal(char_vector);

  std::list<uint16_t> int_list = {2, 4};
  std::list<char> char_list = {'L', 'i', 's', 't'};

  std::cout << "For lists:" << std::endl;
  // Вызов функций для одинаковых контейнеров (структур данных),
  // Вызов функций для других контейнеров (структур данных),
  // но разных типов данных для печати на экране в прямом порядке
  directOrderPrintInTerminal(int_list);
  directOrderPrintInTerminal(char_list);
  // Вызов функций для других контейнеров (структур данных),
  // но разных типов данных для печати на экране в обратном порядке
  reverseOrderPrintInTerminal(int_list);
  reverseOrderPrintInTerminal(char_list);

  return 0;
}

template <typename Data>
void directOrderPrintInTerminal(Data data) {
  for (auto item : data) {
    std::cout << item << " ";
  }
  std::cout << std::endl;
}

template <typename Data>
void reverseOrderPrintInTerminal(Data data) {
  for(auto item: data | std::views::reverse) {
	std::cout << item << " "; 
  }
	std::cout << std::endl;
}
