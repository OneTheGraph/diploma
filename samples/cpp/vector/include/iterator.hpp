// Абстрактный класс для описания сигнатур.
template <class Item>
class Iterator {
 public:
  // Получение первого элемента.
  virtual void first() = 0;
  // Переход к следующему элементу.
  virtual void next() = 0;
  // Функция, которая проверяет находится ли
  // итератор на конечном элементе.
  virtual bool isDone() const = 0;
  // Функция получения текущего элемента.
  virtual Item currentItem() const = 0;
};
