#include <directListIterator.hpp>
#include <iostream>

int main() {
  // Инициализация вектора из стандартной бибилиотеки std
  std::vector<int> vector1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  // Инициализация нашего итератора.
  // В качестве параметра передаем указатель на vector1
  // при помощи оператора взятия адреса "&".
  DirectVectorIterator<int> iter(&vector1);
  // Основной цикл, перебирающий все элементы.
  while (!iter.isDone()) {
    // Вывод элементов.
    std::cout << iter.currentItem() << " ";
    // Переход к следующему элементу.
    iter.next();
  }
  return 0;
}
