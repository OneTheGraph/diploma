import diploma.Main;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

class MainTest {

    @Test
    public void testListRemoveFive() {
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(4);
        list1.add(5);
        list1.add(6);

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.add(6);

        Main.listRemoveFive(list1);

        Assert.assertEquals(list1, list2);
    }

    public void testTestListRemoveFive() {
    }
}