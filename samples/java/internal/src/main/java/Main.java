import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);


    }

    public static <T> void listDirectOrder(ArrayList<T> list) {
        for (T item : list) {
            System.out.print(item + " ");
        }
    }

    public static <T> void listReverseOrder(ArrayList<T> list) {
        // Very bad solution. O(n) = n^3 !
        Collections.reverse(list);
        for (T item : list) {
            System.out.print(item + " ");
        }
        Collections.reverse(list);
    }
}
