# invalidation
В примере показана ситуация инвалидации итератора и способ устранения.

# Запуск проекта
Для запуска проекта необходимо установить `idea`.

`Ubuntu`: `sudo apt install snap java-common && sudo snap install intellij-idea-community --classic --edge` 

# Сборка и запуск:
Откройте проект при помощи `IntelliJ IDEA`. Посе прогрузки нажмите зеленую кнопку play в левой верхней части окна. 
