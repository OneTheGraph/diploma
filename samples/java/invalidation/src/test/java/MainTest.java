import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void invalidationListRemoveFive() {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);

        ArrayList<Integer> list1 = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);

        // > Process 'command '/usr/lib/jvm/java-1.11.0-openjdk-amd64/bin/java'' finished with non-zero exit value 1
        Main.invalidationListRemoveFive(list);

        Assertions.assertEquals(list, list1);
    }
}