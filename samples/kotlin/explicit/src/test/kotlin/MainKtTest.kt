import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MainKtTest {

    @Test
    fun listToStirngTest() {
        val string = "1 2 3 4 5 6 7 8 9 "

        assertEquals(string, listToString())
    }
}