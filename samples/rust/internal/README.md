# implicit
В примере используется внутренний (неявный) итератор для печати элементов `array` и `mut array`. Используются [разрезы(slice)](https://doc.rust-lang.org/std/slice/index.html) 

# Установка
Данный проект требует инструменты языка программирования `Rust`.

Установка в `Ubuntu`, `Debian` (пакетный менеджер `apt`):

`sudo apt install rustc`


Установка в `Arch linux`:

`sudo pacman -S rust`

Возможна установка пакета инструментов (включая компилятор и другие инструменты) при помощи `rustup`:

`curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`

# Запуск
В папке проекта собираем проект и запускаем при помощи `Cargo`:

`cargo run`
